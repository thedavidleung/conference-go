from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    # Create the URL for the request with the city and state
    params = {
        "per_page": 1,
        "query": f" {city} {state}",
    }
    url = "https://api.pexels.com/v1/search"
    # Make the request
    response = requests.get(url, params=params, headers=headers)
    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    params = {
        "q": f"{city},{state}, US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the latitude and longitude from the response
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "metric",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
